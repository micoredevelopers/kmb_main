$('.main-carousel').flickity({
    cellAlign: 'left',
    contain: true,
    pageDots: false,
    prevNextButtons: false,
    draggable: true,
    freeScroll: true,
    scroll: false
});
$('.hamb_container').click(function () {
    $('.global-menu').toggleClass('active');
});

var name_header = document.querySelector(".name_header"), number_header = document.querySelector(".number_header"), mail_header = document.querySelector(".mail_header"),firstBtn = document.querySelector(".first_btn");
var name_footer = document.querySelector(".name_footer"),number_footer = document.querySelector(".number_footer"), mail_footer = document.querySelector(".mail_footer"),second_btn = document.querySelector(".second_btn");
var name_mobile = document.querySelector(".name_mobile"), number_mobile = document.querySelector(".number_mobile"),mail_mobile = document.querySelector(".mail_mobile"),mobile_btn = document.querySelector(".mobile_btn");

function keyNumber(id) {
    id.addEventListener("keypress", function (e) {
        if (e.keyCode >= 48 && e.keyCode <= 57) {
        }
        else {
            e.preventDefault()
        }
    });
}

function successModal(key,name,number,mail){
    var em = /\b[A-Za-z0-9._]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b/;
    key.addEventListener("click", function () {
        if(name.value===""){
            name.style.borderBottom="1px solid #e30e31";
        }
        else{
            name.style.borderBottom="1px solid rgba(0, 0, 0, 0.2)";
        }
        if(number.value===""){
            number.style.borderBottom="1px solid #e30e31";
        }
        else{
            number.style.borderBottom="1px solid rgba(0, 0, 0, 0.2)";
        }
        if(!em.test(mail.value)){
            mail.style.borderBottom="1px solid #e30e31";
        }
        else{
            mail.style.borderBottom="1px solid rgba(0, 0, 0, 0.2)";
        }
        if((name.value)!==""&&(number.value)!==""&&em.test(mail.value)){
            console.log("Message Send");
            $('.successModal').modal('show');
            name.value="";
            number.value="";
            mail.value="";
        }
    });
}
if(firstBtn!==null||mobile_btn!==null) {
    successModal(firstBtn, name_header, number_header, mail_header);
    successModal(mobile_btn, name_mobile, number_mobile, mail_mobile);
}

successModal(second_btn,name_footer,number_footer,mail_footer);
if(number_mobile!==null||number_header!==null) {
    keyNumber(number_mobile);
    keyNumber(number_header);
}
keyNumber(number_footer);
